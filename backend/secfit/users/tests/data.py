test_data_one = {
    "username": "username",
    "email": "name@mail.com",
    "password": "a",
    "password1": "a",
    "phone_number": "12345678",
    "country": "Norway",
    "city": "Trondheim",
    "street_address": "Kongens Gate 1"
}

test_data_two = {
    "username": "username",
    "email": "name@mail.com",
    "password": "",
    "password1": "",
    "phone_number": "12345678",
    "country": "Norway",
    "city": "Trondheim",
    "street_address": "Kongens Gate 1"
}


fields = [
    'username',
    'email',
    'password',
    'password1',
    'phone_number',
    'country',
    'city',
    'street_address'
]

field_values = {
    'valid':
        {
            "username": "username_two",
            "email": "name_two@mail.com",
            "password": "password",
            "password1": "password",
            "phone_number": "12345678",
            "country": "Norway",
            "city": "Trondheim",
            "street_address": "Kongens Gate 1"
        },
    'invalid': {
        'username': 'User name',
        'email': 'not_email',
        'password': None,
        'password1': None,
        'phone_number': 'abcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyz',
        'country': 'abcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyz',
        'city': 'abcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyz',
        'street_address': 'abcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyzabcdefghijklmnopqrstuvxyz',
    },
    'blank': {
        'username': '',
        'email': '',
        'password': '',
        'password1': '',
        'phone_number': '',
        'country': '',
        'city': '',
        'street_address': '',
    }
}

test_cases = [
        {
            "username": "invalid",
            "password": "blank",
            "password1": "valid",
            "email": "invalid",
            "phone_number": "valid",
            "country": "blank",
            "city": "blank",
            "street_address": "invalid"
        },
        {
            "username": "invalid",
            "password": "valid",
            "password1": "blank",
            "email": "valid",
            "phone_number": "blank",
            "country": "blank",
            "city": "invalid",
            "street_address": "valid"
        },
        {
            "username": "invalid",
            "password": "valid",
            "password1": "blank",
            "email": "blank",
            "phone_number": "invalid",
            "country": "valid",
            "city": "blank",
            "street_address": "invalid"
        },
        {
            "username": "invalid",
            "password": "blank",
            "password1": "blank",
            "email": "invalid",
            "phone_number": "valid",
            "country": "blank",
            "city": "invalid",
            "street_address": "valid"
        },
        {
            "username": "invalid",
            "password": "blank",
            "password1": "valid",
            "email": "valid",
            "phone_number": "blank",
            "country": "invalid",
            "city": "valid",
            "street_address": "blank"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "blank",
            "email": "blank",
            "phone_number": "invalid",
            "country": "blank",
            "city": "valid",
            "street_address": "blank"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "blank",
            "email": "invalid",
            "phone_number": "blank",
            "country": "valid",
            "city": "blank",
            "street_address": "valid"
        },
        {
            "username": "valid",
            "password": "valid",
            "password1": "valid",
            "email": "valid",
            "phone_number": "blank",
            "country": "valid",
            "city": "invalid",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "valid",
            "password1": "blank",
            "email": "valid",
            "phone_number": "valid",
            "country": "invalid",
            "city": "invalid",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "blank",
            "email": "blank",
            "phone_number": "blank",
            "country": "valid",
            "city": "valid",
            "street_address": "invalid"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "blank",
            "email": "valid",
            "phone_number": "invalid",
            "country": "invalid",
            "city": "blank",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "valid",
            "email": "blank",
            "phone_number": "valid",
            "country": "valid",
            "city": "invalid",
            "street_address": "invalid"
        },
        {
            "username": "blank",
            "password": "valid",
            "password1": "blank",
            "email": "blank",
            "phone_number": "blank",
            "country": "blank",
            "city": "valid",
            "street_address": "valid"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "valid",
            "email": "invalid",
            "phone_number": "invalid",
            "country": "blank",
            "city": "blank",
            "street_address": "blank"
        },
        {
            "username": "invalid",
            "password": "blank",
            "password1": "valid",
            "email": "blank",
            "phone_number": "invalid",
            "country": "invalid",
            "city": "valid",
            "street_address": "valid"
        },
        {
            "username": "invalid",
            "password": "valid",
            "password1": "valid",
            "email": "valid",
            "phone_number": "blank",
            "country": "blank",
            "city": "blank",
            "street_address": "invalid"
        },
        {
            "username": "invalid",
            "password": "blank",
            "password1": "blank",
            "email": "blank",
            "phone_number": "blank",
            "country": "invalid",
            "city": "invalid",
            "street_address": "valid"
        },
        {
            "username": "invalid",
            "password": "valid",
            "password1": "blank",
            "email": "invalid",
            "phone_number": "invalid",
            "country": "valid",
            "city": "valid",
            "street_address": "blank"
        },
        {
            "username": "valid",
            "password": "valid",
            "password1": "valid",
            "email": "invalid",
            "phone_number": "blank",
            "country": "invalid",
            "city": "valid",
            "street_address": "blank"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "blank",
            "email": "valid",
            "phone_number": "blank",
            "country": "valid",
            "city": "blank",
            "street_address": "invalid"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "valid",
            "email": "blank",
            "phone_number": "invalid",
            "country": "blank",
            "city": "invalid",
            "street_address": "valid"
        },
        {
            "username": "valid",
            "password": "valid",
            "password1": "blank",
            "email": "invalid",
            "phone_number": "valid",
            "country": "blank",
            "city": "valid",
            "street_address": "blank"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "blank",
            "email": "valid",
            "phone_number": "blank",
            "country": "invalid",
            "city": "blank",
            "street_address": "invalid"
        },
        {
            "username": "valid",
            "password": "valid",
            "password1": "valid",
            "email": "blank",
            "phone_number": "invalid",
            "country": "valid",
            "city": "blank",
            "street_address": "valid"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "blank",
            "email": "blank",
            "phone_number": "valid",
            "country": "blank",
            "city": "invalid",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "valid",
            "email": "invalid",
            "phone_number": "blank",
            "country": "valid",
            "city": "invalid",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "blank",
            "email": "valid",
            "phone_number": "invalid",
            "country": "blank",
            "city": "valid",
            "street_address": "invalid"
        },
        {
            "username": "blank",
            "password": "valid",
            "password1": "blank",
            "email": "blank",
            "phone_number": "valid",
            "country": "invalid",
            "city": "blank",
            "street_address": "valid"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "valid",
            "email": "blank",
            "phone_number": "blank",
            "country": "valid",
            "city": "invalid",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "valid",
            "password1": "blank",
            "email": "invalid",
            "phone_number": "blank",
            "country": "blank",
            "city": "valid",
            "street_address": "invalid"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "valid",
            "email": "valid",
            "phone_number": "invalid",
            "country": "blank",
            "city": "blank",
            "street_address": "valid"
        },
        {
            "username": "blank",
            "password": "valid",
            "password1": "blank",
            "email": "blank",
            "phone_number": "valid",
            "country": "invalid",
            "city": "blank",
            "street_address": "blank"
        }
]
