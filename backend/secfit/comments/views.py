"""Comments views."""
from django.db.models import Q
from rest_framework import generics, mixins, permissions
from rest_framework.filters import OrderingFilter

from comments.models import Comment, Like
from comments.permissions import IsCommentVisibleToUser
from comments.serializers import CommentSerializer, LikeSerializer
from workouts.permissions import IsOwner, IsReadOnly


class CommentList(
    mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    """List all comments, or create a new comment."""
    serializer_class = CommentSerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [OrderingFilter]
    ordering_fields = ["timestamp"]

    def get(self, request, *args, **kwargs):
        """Get a list of all comments."""
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Create a new comment."""
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        """Create a new comment."""
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        """Get the queryset for the list of comments."""
        workout_pk = self.kwargs.get("pk")
        queryset = Comment.objects.none()

        if workout_pk:
            queryset = Comment.objects.filter(workout=workout_pk)
        elif self.request.user:
            # The code below is kind of duplicate of the one in ./permissions.py
            # We should replace it with a better solution.
            # Or maybe not.

            queryset = Comment.objects.filter(
                Q(workout__visibility="PU")
                | Q(owner=self.request.user)
                | (
                    Q(workout__visibility="CO")
                    & Q(workout__owner__coach=self.request.user)
                )
                | Q(workout__owner=self.request.user)
            ).distinct()

        return queryset


class CommentDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    """Retrieve, update or delete a comment instance."""
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = [
        permissions.IsAuthenticated & IsCommentVisibleToUser & (IsOwner | IsReadOnly)
    ]

    def get(self, request, *args, **kwargs):
        """Get a comment instance."""
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        """Update a comment instance."""
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """Delete a comment instance."""
        return self.destroy(request, *args, **kwargs)


class LikeList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    """List all likes, or create a new like."""
    serializer_class = LikeSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        """Get a list of all likes."""
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Create a new like."""
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        """Create a new like."""
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        """Get the queryset for the list of likes."""
        return Like.objects.filter(owner=self.request.user)


class LikeDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    """Retrieve, update or delete a like instance."""
    queryset = Like.objects.all()
    serializer_class = LikeSerializer
    permission_classes = [permissions.IsAuthenticated]
    _Detail = []

    def get(self, request, *args, **kwargs):
        """Get a like instance."""
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        """Update a like instance."""
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """Delete a like instance."""
        return self.destroy(request, *args, **kwargs)
