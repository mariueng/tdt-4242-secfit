from users.models import User
from users.serializers import UserSerializer
from rest_framework import serializers
from django.test import TestCase, Client
from rest_framework.request import Request
from rest_framework.test import APIRequestFactory


class UserSerializerTest(TestCase):

    def setUp(self):
        self.user_attributes = {
            "id": 1,
            "email": "test@test.com",
            "username": "test",
            "password": "test_123456",
            "phone_number": 123456789,
            "country": "Norway",
            "city": "Trondheim",
            "street_address": "Kongens gate 1",
        }

        self.serializer_data = {
            "url": "https://link.com",
            "id": 1,
            "email": "test@test.com",
            "username": "test",
            "password": "test_123456",
            "password1": "test_123456",
            "athletes": "athletes",
            "phone_number": 123456789,
            "country": "Norway",
            "city": "Trondheim",
            "street_address": "Kongens gate 1",
            "coach": "coach",
            "workouts": "workouts",
            "coach_files": "coach_files",
            "athlete_files": "athlete_files",
        }

        self.client = Client()

        factory = APIRequestFactory()
        request = factory.get('/')
        serializer_context = {
            'request': Request(request),
        }

        self.user = User.objects.create(**self.user_attributes)
        self.serializer = UserSerializer(instance=self.user,
                                         context=serializer_context)

    def test_contains_expected_fields(self):
        data = self.serializer.data
        self.assertEqual(set(data.keys()), set(["url",
                                                "id",
                                                "email",
                                                "username",
                                                "athletes",
                                                "phone_number",
                                                "country",
                                                "city",
                                                "street_address",
                                                "coach",
                                                "workouts",
                                                "coach_files",
                                                "athlete_files"]))

    def test_validate_password(self):
        # Assert that correct password yields same password
        self.assertEqual(self.user.password, self.serializer.validate_password(self.user.password))

    def test_incorrect_password_raises_exception(self):
        # Assert that ValidationError is raised for incorrect password
        # Change to invalid password
        self.serializer.password = '123'
        with self.assertRaises(serializers.ValidationError):
            self.serializer.validate_password(self.serializer.password)
            raise serializers.ValidationError

    def test_create_method(self):
        # Assert that user was created correctly
        self.assertEqual(self.user_attributes['username'], self.user.username)
        self.assertEqual(self.user_attributes['password'], self.user.password)
