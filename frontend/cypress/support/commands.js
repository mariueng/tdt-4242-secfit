// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
// import 'cypress-file-upload';


Cypress.Commands.add("login", () => {
    cy.visit('/')
    cy.visit('login.html')
    cy.get('#form-login').within(($form) => {
        cy.fixture('auth.json').then((users) => {
            cy.get('input[name="username"]').type(users.admin.name)
            cy.get('input[name="password"]').type(users.admin.password)
        })
    })
    cy.get('#btn-login').click()

    cy.location().should((loc) => {
        expect(loc.pathname).to.eq('/workouts.html')
    })

    expect(cy.getCookie("access")).to.exist;
    expect(cy.getCookie("refresh")).to.exist;
})

Cypress.Commands.add('getToken', () => {
    cy.fixture('auth.json').then((users) => {
        cy
            .request('POST','api/token/', { username:users.admin.name, password:users.admin.password})
            .then((response) => {
                return response.body.access
            })
    })
})

Cypress.Commands.add("django_login", () => {
    cy.clearCookie("sessionid")
    cy.visit('/admin/login/?next=/admin/')
    cy.get('#login-form').within(($form) => {
        cy.fixture('auth.json').then((users) => {
            cy.get('input[name="username"]').type(users.admin.name)
            cy.get('input[name="password"]').type(users.admin.password)
            cy.get('input[type="submit"]').click()
        })
    })

    cy.location().should((loc) => {
        expect(loc.pathname).to.eq('/admin/')
    })

    expect(cy.getCookie("sessionid")).to.exist;
})
Cypress.Commands.add('register', () => {
    cy.visit('/')
    cy.visit('register.html')
})
