describe("Boundary testing for new workouts", () => {
    beforeEach(() => {
      cy.login();
      Cypress.Cookies.preserveOnce("access", "refresh");
    });
  
    it("Valid - Create new workout", () => {
      cy.get("#btn-create-workout").click();
  
      cy.get("#inputName").type("Valid workout");
      cy.get("#inputDateTime").type("2022-03-30T12:00");
      cy.get("#inputVisibility").select("PU");
      cy.get("#inputNotes").type("text");
  
      cy.scrollTo("bottom");
  
      cy.get("#div-exercises").within(($form) => {
        cy.get('select[name="type"]').select("1", { force: true });
        cy.get('input[name="sets"]').type("1", { force: true });
        cy.get('input[name="number"]').type("1", { force: true });
      });
  
      cy.get("#btn-ok-workout").click();
      cy.url().should("include", "/workouts.html");
    });
  
    it("Valid - Create workout with only required fields", () => {
      cy.get("#btn-create-workout").click();
  
      cy.get("#inputName").type("Valid workout");
      cy.get("#inputDateTime").type("2022-03-30T12:00");
      cy.get("#inputVisibility").select("PU");
      cy.get("#inputNotes").type("text");
  
      cy.get("#btn-ok-workout").click();
      cy.url().should("include", "/workouts.html");
    });
  
    it("Valid - Create workout with several exercises ", () => {
      cy.get("#btn-create-workout").click();
  
      cy.get("#inputName").type("Valid workout");
      cy.get("#inputDateTime").type("2022-03-30T12:00");
      cy.get("#inputVisibility").select("PU");
      cy.get("#inputNotes").type("text");
  
      cy.scrollTo("bottom");
  
      cy.get("#div-exercises").within(($form) => {
        cy.get('select[name="type"]').select("1", { force: true });
        cy.get('input[name="sets"]').type("1", { force: true });
        cy.get('input[name="number"]').type("1", { force: true });
      });
  
      cy.get('#btn-add-exercise').click({force:true})
  
      cy.get(".div-exercise-container").eq(1).within(($form) => {
          cy.get('select[name="type"]').select("1", { force: true });
          cy.get('input[name="sets"]').type("10", { force: true });
          cy.get('input[name="number"]').type("10", { force: true });
        });
  
      cy.get("#btn-ok-workout").click();
      cy.url().should('include', '/workouts.html');
    });
  
    it("Invalid - Create workout without name", () => {
      cy.get("#btn-create-workout").click();
  
      cy.get("#inputDateTime").type("2022-03-30T12:00");
      cy.get("#inputVisibility").select("PU");
      cy.get("#inputNotes").type("text");
  
      cy.scrollTo("bottom");
  
      cy.get("#div-exercises").within(($form) => {
        cy.get('select[name="type"]').select("1", { force: true });
        cy.get('input[name="sets"]').type("1", { force: true });
        cy.get('input[name="number"]').type("1", { force: true });
      });
      cy.get("#btn-ok-workout").click();
      cy.contains('This field may not be blank');
    });
  
    it("Invalid - Create workout with more than max chars in name", () => {
      cy.get("#btn-create-workout").click();
  
      cy.get("#inputName").type("t".repeat(101));
      cy.get("#inputDateTime").type("2022-03-30T12:00");
      cy.get("#inputVisibility").select("PU");
      cy.get("#inputNotes").type("text");
      cy.scrollTo("bottom");
      cy.get("#div-exercises").within(($form) => {
        cy.get('select[name="type"]').select("1", { force: true });
        cy.get('input[name="sets"]').type("1", { force: true });
        cy.get('input[name="number"]').type("1", { force: true });
      });
  
      cy.get("#btn-ok-workout").click();
      cy.contains("Ensure this field has no more than 100 characters");
    });
  
    it("Valid - Create workout with max length name", () => {
      cy.get("#btn-create-workout").click();
  
      cy.get("#inputName").type("t".repeat(100));
      cy.get("#inputDateTime").type("2022-03-30T12:00");
      cy.get("#inputVisibility").select("PU");
      cy.get("#inputNotes").type("text");
  
      cy.scrollTo("bottom");
  
      cy.get("#div-exercises").within(($form) => {
        cy.get('select[name="type"]').select("1", { force: true });
        cy.get('input[name="sets"]').type("1", { force: true });
        cy.get('input[name="number"]').type("1", { force: true });
      });
  
      cy.get("#btn-ok-workout").click();
      cy.url().should("include", "/workouts.html");
    });
  
    it("Valid - Create workout with max date", () => {
      cy.get("#btn-create-workout").click();
      cy.get("#inputName").type("Valid workout");
      cy.get("#inputDateTime").type("9999-12-31T23:59");
      cy.get("#inputVisibility").select("PU");
      cy.get("#inputNotes").type("text");
  
      cy.scrollTo("bottom");
  
      cy.get("#div-exercises").within(($form) => {
        cy.get('select[name="type"]').select("1", { force: true });
        cy.get('input[name="sets"]').type("1", { force: true });
        cy.get('input[name="number"]').type("1", { force: true });
      });
      cy.get("#btn-ok-workout").click();
      cy.url().should("include", "/workouts.html");
    });
  
    it("Valid - Create workout with min date", () => {
      cy.get("#btn-create-workout").click();
      cy.get("#inputName").type("Valid workout");
      cy.get("#inputDateTime").type("0002-01-01T00:00");
      cy.get("#inputVisibility").select("PU");
      cy.get("#inputNotes").type("text");
  
      cy.scrollTo("bottom");
  
      cy.get("#div-exercises").within(($form) => {
        cy.get('select[name="type"]').select("1", { force: true });
        cy.get('input[name="sets"]').type("1", { force: true });
        cy.get('input[name="number"]').type("1", { force: true });
      });
      cy.get("#btn-ok-workout").click();
      cy.url().should("include", "/workouts.html");
    });
  
    it("Invalid - Create workout with less than min date", () => {
      cy.get("#btn-create-workout").click();
      cy.get("#inputName").type("Invalid workout");
      cy.get("#inputDateTime").type("0001-01-01T00:00");
      cy.get("#inputVisibility").select("PU");
      cy.get("#inputNotes").type("text");
  
      cy.scrollTo("bottom");
  
      cy.get("#div-exercises").within(($form) => {
        cy.get('select[name="type"]').select("1", { force: true });
        cy.get('input[name="sets"]').type("1", { force: true });
        cy.get('input[name="number"]').type("1", { force: true });
      });
      cy.get("#btn-ok-workout").click();
      cy.contains('Datetime has wrong format');
    });
  
    it("Invalid - Create workout with blank note", () => {
      cy.get("#btn-create-workout").click();
  
      cy.get("#inputName").type("Invalid workout");
      cy.get("#inputDateTime").type("2022-03-30T12:00");
      cy.get("#inputVisibility").select("PU");
      cy.get("#inputNotes")
  
      cy.scrollTo("bottom");
  
      cy.get("#div-exercises").within(($form) => {
        cy.get('select[name="type"]').select("1", { force: true });
        cy.get('input[name="sets"]').type("1", { force: true });
        cy.get('input[name="number"]').type("1", { force: true });
      });
  
      cy.get("#btn-ok-workout").click();
      cy.contains('This field may not be blank')
    });
  });
  