"""Views for the users app."""
from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework import generics, mixins, permissions
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import (
    IsAuthenticatedOrReadOnly,
)

from users.models import AthleteFile, Offer
from users.permissions import IsAthlete, IsCoach, IsCurrentUser
from users.serializers import (
    AthleteFileSerializer,
    OfferSerializer,
    UserGetSerializer,
    UserPutSerializer,
    UserSerializer,
)
from workouts.mixins import CreateListModelMixin
from workouts.permissions import IsOwner, IsReadOnly


class UserList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    """List all users, or create a new user."""

    serializer_class = UserSerializer
    users = []
    admins = []

    def get(self, request, *args, **kwargs):
        """Get a list of all users.

        If the user is an admin, return all users.
        If the user is a coach, return all users that are athletes.
        If the user is an athlete, return all users that are athletes.
        """

        self.serializer_class = UserGetSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Create a new user."""

        return self.create(request, *args, **kwargs)

    def get_queryset(self):
        """Get the queryset for the list of users."""

        queryset = get_user_model().objects.all()

        if self.request.user:
            # Return the currently logged in user
            status = self.request.query_params.get("user", None)
            if status and status == "current":
                queryset = get_user_model().objects.filter(pk=self.request.user.pk)

        return queryset


class UserDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    """Retrieve, update or delete a user instance."""

    lookup_field_options = ["pk", "username"]
    serializer_class = UserSerializer
    queryset = get_user_model().objects.all()
    permission_classes = [permissions.IsAuthenticated & (IsCurrentUser | IsReadOnly)]

    def get_object(self):
        """Get the object for the detail view."""

        for field in self.lookup_field_options:
            if field in self.kwargs:
                self.lookup_field = field
                break

        return super().get_object()

    def get(self, request, *args, **kwargs):
        """Get a user instance."""

        self.serializer_class = UserGetSerializer
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """Delete a user instance."""

        return self.destroy(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        """Update a user instance."""

        self.serializer_class = UserPutSerializer
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        """Update a user instance."""

        return self.partial_update(request, *args, **kwargs)


class OfferList(
    mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    """List all offers, or create a new offer."""

    permission_classes = [IsAuthenticatedOrReadOnly]
    serializer_class = OfferSerializer

    def get(self, request, *args, **kwargs):
        """Get a list of all offers."""
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Create a new offer."""
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        """Set the owner of the offer."""
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        """Get the queryset for the list of offers."""
        result = Offer.objects.none()

        if self.request.user:
            queryset = Offer.objects.filter(
                Q(owner=self.request.user) | Q(recipient=self.request.user)
            ).distinct()
            query_parameters = self.request.query_params
            user = self.request.user

            # filtering by status (if provided)
            status = query_parameters.get("status", None)
            if status is not None and self.request is not None:
                queryset = queryset.filter(status=status)
                if query_parameters.get("status", None) is None:
                    queryset = Offer.objects.filter(Q(owner=user)).distinct()

            # filtering by category (sent or received)
            category = query_parameters.get("category", None)
            if category is not None and query_parameters is not None:
                if category == "sent":
                    queryset = queryset.filter(owner=user)
                elif category == "received":
                    queryset = queryset.filter(recipient=user)
            return queryset

        return result


class OfferDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    """Retrieve, update or delete a offer instance."""

    permission_classes = [IsAuthenticatedOrReadOnly]
    queryset = Offer.objects.all()
    serializer_class = OfferSerializer

    def get(self, request, *args, **kwargs):
        """Get a offer instance."""
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        """Update a offer instance."""
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        """Update a offer instance."""
        return self.partial_update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """Delete a offer instance."""
        return self.destroy(request, *args, **kwargs)


class AthleteFileList(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    CreateListModelMixin,
    generics.GenericAPIView,
):
    """List all athlete files, or create a new athlete file."""

    queryset = AthleteFile.objects.all()
    serializer_class = AthleteFileSerializer
    permission_classes = [permissions.IsAuthenticated & (IsAthlete | IsCoach)]
    parser_classes = [MultiPartParser, FormParser]

    def get(self, request, *args, **kwargs):
        """Get a list of all athlete files."""
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Create a new athlete file."""
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        """Set the owner of the athlete file."""
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        """Get the queryset for the list of athlete files."""
        queryset = AthleteFile.objects.none()

        if self.request.user:
            queryset = AthleteFile.objects.filter(
                Q(athlete=self.request.user) | Q(owner=self.request.user)
            ).distinct()

        return queryset


class AthleteFileDetail(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.GenericAPIView,
):
    """Retrieve, update or delete a athlete file instance."""

    queryset = AthleteFile.objects.all()
    serializer_class = AthleteFileSerializer
    permission_classes = [permissions.IsAuthenticated & (IsAthlete | IsOwner)]

    def get(self, request, *args, **kwargs):
        """Get a athlete file instance."""
        return self.retrieve(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """Delete a athlete file instance."""
        return self.destroy(request, *args, **kwargs)
