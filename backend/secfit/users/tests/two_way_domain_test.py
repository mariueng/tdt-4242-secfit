import uuid
from django.test import TestCase, Client
from users.tests.data import field_values, test_cases


class UserRegistration2WayTestCase(TestCase):

    def setUp(self):
        print("Running two way user registration")
        self.client = Client()

    def test_two_way(self):
        
        for testcase in test_cases:
            request = {}
            invalid = False
            for key, value in testcase.items():
                request[key] = field_values[value][key]

                if key == 'username' and value == 'valid':
                    request['username'] = request['username'] + str(uuid.uuid4())
                if value == 'invalid':
                    invalid = True
                elif key == ('username' or 'password' or 'password1') and value == 'blank':
                    invalid = True

            # Checking if the repsonse should return status code 400 or 201
            actual_status_code = 400 if invalid else 201
            expected_status_code = self.client.post('/api/users/', request).status_code
            self.assertEqual(expected_status_code, actual_status_code)
