"""
Tests for the workouts application.
"""
from django.test import TestCase
from datetime import datetime
from django.utils import timezone

from users.models import User
import workouts.permissions as ps
from workouts.models import Workout
from rest_framework.test import APIRequestFactory

from comments.models import Comment


class UserPermissionTest(TestCase):
    def setUp(self):

        self.coach_attr = {
            "id": 2,
            "email": "coach@coach.com",
            "username": "coach",
            "password": "test_123456",
            "phone_number": 123456789,
            "country": "Norway",
            "city": "Trondheim",
            "street_address": "Kongens gate 1",
        }
        self.coach = User.objects.create(**self.coach_attr)

        self.athlete_attr = {
            "id": 1,
            "email": "athlte@athlete.com",
            "username": "athlete",
            "password": "test_123456",
            "phone_number": 123456789,
            "coach": self.coach,
            "country": "Norway",
            "city": "Trondheim",
            "street_address": "Kongens gate 1",
        }

        self.athlete = User.objects.create(**self.athlete_attr)
        self.public_workout = Workout.objects.create(
            name="public_workout",
            date=timezone.now(),
            notes="text",
            owner=self.athlete,
            visibility="PU",
        )
        self.coach_workout = Workout.objects.create(
            name="coach_workout",
            date=timezone.now(),
            notes="text",
            owner=self.athlete,
            visibility="CO",
        )

        self.comment_public = Comment.objects.create(
            owner=self.athlete, workout=self.public_workout, content="content"
        )
        self.comment_coach = Comment.objects.create(
            owner=self.athlete, workout=self.coach_workout, content="content"
        )

        # Set up base request
        self.factory = APIRequestFactory()
        self.request = self.factory.post("", None)

        # Instantiate permission objects
        self.is_owner = ps.IsOwner()
        self.is_owner_of_workout = ps.IsOwnerOfWorkout()
        self.is_coach_and_visible_to_coach = ps.IsCoachAndVisibleToCoach()
        self.is_coach_of_workout_and_visible_to_coach = (
            ps.IsCoachOfWorkoutAndVisibleToCoach()
        )
        self.is_public = ps.IsPublic()
        self.is_workout_public = ps.IsWorkoutPublic()
        self.is_read_only = ps.IsReadOnly()

    def test_is_owner_true(self):
        # Assert that owner has permission
        self.request.user = self.athlete
        self.assertTrue(
            self.is_owner.has_object_permission(
                request=self.request, view=None, obj=self.public_workout
            )
        )

    def test_is_owner_false(self):
        # Assert that a non owner does not have permission
        self.request.user = self.coach
        self.assertFalse(
            self.is_owner.has_object_permission(
                request=self.request, view=None, obj=self.public_workout
            )
        )

    def test_is_owner_of_workout_has_permission_true_owner(self):
        # Check that requesting user is owner of new object
        data = {"workout": "1/1/1/1"}
        request = self.factory.post("", {"workout": self.public_workout})
        request.data = data
        request.user = self.athlete
        self.assertTrue(
            self.is_owner_of_workout.has_permission(request=request, view=None)
        )

    def test_is_owner_of_workout_has_permission_no_workout(self):
        # Check that requesting user is owner of new object
        data = {}
        request = self.factory.post("", {"workout": self.public_workout})
        request.data = data
        request.user = self.athlete
        self.assertFalse(
            self.is_owner_of_workout.has_permission(request=request, view=None)
        )

    def test_is_owner_of_workout_has_permission_get(self):
        # Check that requesting user is owner of new object
        request = self.factory.get("", {"workout": self.public_workout})
        self.assertTrue(
            self.is_owner_of_workout.has_permission(request=request, view=None)
        )

    def test_is_owner_of_workout_has_object_permission(self):
        # Check that requesting user is owner of new object
        self.request.user = self.athlete
        self.assertTrue(
            self.is_owner_of_workout.has_object_permission(
                request=self.request, view=None, obj=self.comment_public
            )
        )

    def test_is_coach_and_visible_to_coach_public_workout(self):
        # Assert that owner's coach can view workout: Public visibilty
        self.request.user = self.coach
        self.assertTrue(
            self.is_coach_and_visible_to_coach.has_object_permission(
                request=self.request, view=None, obj=self.public_workout
            )
        )

    def test_is_coach_and_visible_to_coach_coach_workout(self):
        # Assert that owner's coach can view workout: Coach visbility
        self.request.user = self.coach
        self.assertTrue(
            self.is_coach_and_visible_to_coach.has_object_permission(
                request=self.request, view=None, obj=self.coach_workout
            )
        )

    def test_is_coach_of_workout_and_visible_to_coach_publiic(self):
        # Assert that coach has permission to view workout from comment: public visibilty
        self.request.user = self.coach
        self.assertTrue(
            self.is_coach_of_workout_and_visible_to_coach.has_object_permission(
                request=self.request, view=None, obj=self.comment_public
            )
        )

    def test_is_coach_of_workout_and_visible_to_coach_publiic(self):
        # Assert that coach has permission to view workout from comment: public visibilty
        self.request.user = self.coach
        self.assertTrue(
            self.is_coach_of_workout_and_visible_to_coach.has_object_permission(
                request=self.request, view=None, obj=self.comment_coach
            )
        )

    def test_is_public(self):
        self.assertTrue(
            self.is_public.has_object_permission(
                request=self.request, view=None, obj=self.public_workout
            )
        )

    def test_is_workout_public(self):
        self.assertTrue(
            self.is_workout_public.has_object_permission(
                request=self.request, view=None, obj=self.comment_public
            )
        )

    def test_is_read_only_post(self):
        self.request = self.factory.post("", None)
        self.assertFalse(
            self.is_read_only.has_object_permission(
                request=self.request, view=None, obj=None
            )
        )

    def test_is_read_only_get(self):
        self.request = self.factory.get("", None)
        self.assertTrue(
            self.is_read_only.has_object_permission(
                request=self.request, view=None, obj=None
            )
        )
