describe("Integration test for FR5", () => {
    beforeEach(() => {
        cy.login();
        Cypress.Cookies.preserveOnce("access", "refresh");
    });

    it('Verify that public workout is visible for athlete', () => {
        cy.visit('/workouts.html')
        cy.contains("Public Workouts").click();
        cy.get('#div-content').children().first().then((cont) => {
            cy.get('#div-content').then((content)=>{
                const id = content.children().first().attr('href').split("=")[1]
                cy.intercept(`/api/workouts/${id}/`).as('workout')
                cy.get('#div-content').children().first().click()
                cy.wait('@workout').its('response.statusCode').should('eq',200)
                cy.get('#inputName').should('exist')
                cy.get('#inputDateTime').should('exist')
                cy.get('#inputOwner').should('exist')
                cy.get('#inputVisibility').should('exist')
                cy.get('#inputNotes').should('exist')
                cy.get('#uploaded-files').should('exist')
                cy.get('#div-comment-row').should('exist')

            })
        })
    })

});
