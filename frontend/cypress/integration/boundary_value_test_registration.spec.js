describe("Register user", () => {

    it("Register only required fields", () => {
      cy.register();
      cy.get("#form-register-user").within(($form) => {
        cy.get('input[name="username"]').type("username" + Date.now());
        cy.get('input[name="password"]').type("password123456");
        cy.get('input[name="password1"]').type("password123456");
      });
      cy.get("#btn-create-account").click();
      cy.url().should("include", "/workouts.html");
    });

    it("Register with blank fields", () => {
      cy.register();
      cy.get("#btn-create-account").click();
      cy.get(".alert").contains("Registration failed!");
    });

    it("Register with valid input", () => {
      cy.register();
      cy.get("#form-register-user").within(($form) => {
        cy.get('input[name="username"]').type("username" + Date.now());
        cy.get('input[name="email"]').type("username@mail.com");
        cy.get('input[name="password"]').type("123456789");
        cy.get('input[name="password1"]').type("123456789");
        cy.get('input[name="phone_number"]').type("12345678");
        cy.get('input[name="country"]').type("Norge");
        cy.get('input[name="city"]').type("Trondheim");
        cy.get('input[name="street_address"]').type("Kongens Gate 1");
      });
      cy.get("#btn-create-account").click();
      cy.url().should("include", "/workouts.html");
    });

    it("Register with invalid username", () => {
      cy.register();
      cy.get("#form-register-user").within(($form) => {
        cy.get('input[name="username"]').type("usernamefnjkljdfjjjjjjjjjjdfjjfdsjkaklfjbdsjflljnfjksdlnfljesfdshuafobnejsklfnhejsbfsjleabfjshrlbfejiufbesklaejfeiwubfueiorwpheufryuobwffnrjlnafjhersbfjlesnajlfjefesbfjhlrhbfjlsdnfjsbkafjebsfjkbesfjhkbejfkaesjfhjldsfjfskføweiilufhjsoæøafberhkfjljselfbhkjbfnskjdjfjhdjfksdjfjfjdjfjjjfjfjsdaklfnøalwkenfblwoifwlndeflabhrføwæoekfsjhbalieøfslakfn ashlefhrwfjsef" + Date.now());
        cy.get('input[name="email"]').type("username@mail.com");
        cy.get('input[name="password"]').type("123456789");
        cy.get('input[name="password1"]').type("123456789");
        cy.get('input[name="phone_number"]').type("12345678");
        cy.get('input[name="country"]').type("Norge");
        cy.get('input[name="city"]').type("Trondheim");
        cy.get('input[name="street_address"]').type("Kongens Gate 1");
      });
      cy.get("#btn-create-account").click();
      cy.contains("Registration failed!")
    });
  
    it("Register without username", () => {
      cy.register();
      cy.get("#form-register-user").within(($form) => {
        cy.get('input[name="email"]').type("username@mail.com" + Date.now());
        cy.get('input[name="password"]').type("123456789");
        cy.get('input[name="password1"]').type("123456789");
        cy.get('input[name="phone_number"]').type("12345678");
        cy.get('input[name="country"]').type("Norge");
        cy.get('input[name="city"]').type("Trondheim");
        cy.get('input[name="street_address"]').type("Kongens Gate 1");
      });
      cy.get("#btn-create-account").click();
      cy.contains("Registration failed!");
    });
  
    it("Register without password", () => {
      cy.register();
      cy.get("#form-register-user").within(($form) => {
        cy.get('input[name="username"]').type("username" + Date.now());
        cy.get('input[name="email"]').type("username@mail.com");
        cy.get('input[name="password1"]').type("123456789");
        cy.get('input[name="phone_number"]').type("12345678");
        cy.get('input[name="country"]').type("Norge");
        cy.get('input[name="city"]').type("Trondheim");
        cy.get('input[name="street_address"]').type("Kongens Gate 1");
      });
      cy.get("#btn-create-account").click();
      cy.contains("Registration failed!");
    });
  
    it("Register without password1", () => {
      cy.register();
      cy.get("#form-register-user").within(($form) => {
        cy.get('input[name="username"]').type("username" + Date.now());
        cy.get('input[name="email"]').type("username@mail.com");
        cy.get('input[name="password"]').type("123456789");
        cy.get('input[name="phone_number"]').type("12345678");
        cy.get('input[name="country"]').type("Norge");
        cy.get('input[name="city"]').type("Trondheim");
        cy.get('input[name="street_address"]').type("Kongens Gate 1");
      });
      cy.get("#btn-create-account").click();
      cy.contains("Registration failed!");
    });
  
    it("Register with invalid email", () => {
      cy.register();
      cy.get("#form-register-user").within(($form) => {
        cy.get('input[name="username"]').type("username" + Date.now());
        cy.get('input[name="email"]').type("usernamemailcom");
        cy.get('input[name="password"]').type("123456789");
        cy.get('input[name="password1"]').type("123456789");
        cy.get('input[name="phone_number"]').type("12345678");
        cy.get('input[name="country"]').type("Norge");
        cy.get('input[name="city"]').type("Trondheim");
        cy.get('input[name="street_address"]').type("Kongens Gate 1");
      });
      cy.get("#btn-create-account").click();
      cy.get(".alert").contains("Registration failed!");
    });
  
    it("Valid with no city", () => {
      cy.register();
      cy.get("#form-register-user").within(($form) => {
        cy.get('input[name="username"]').type("username" + Date.now());
        cy.get('input[name="email"]').type("username@mail.com");
        cy.get('input[name="password"]').type("123456789");
        cy.get('input[name="password1"]').type("123456789");
        cy.get('input[name="phone_number"]').type("12345678");
        cy.get('input[name="country"]').type("Norge");
        cy.get('input[name="street_address"]').type("Kongens Gate 1");
      });
      cy.get("#btn-create-account").click();
      cy.url().should("include", "/workouts.html");
    });
  
    it("Too long country", () => {
      cy.register();
      cy.get("#form-register-user").within(($form) => {
        cy.get('input[name="username"]').type("username" + Date.now());
        cy.get('input[name="email"]').type("username@mail.com");
        cy.get('input[name="password"]').type("123456789");
        cy.get('input[name="password1"]').type("123456789");
        cy.get('input[name="phone_number"]').type("12345678");
        cy.get('input[name="country"]').type("N".repeat(51));
        cy.get('input[name="city"]').type("Trondheim");
        cy.get('input[name="street_address"]').type("Kongens Gate 1");
      });
      cy.get("#btn-create-account").click();
      cy.contains("Registration failed!");
    });
    
    it("Too long phone number", () => {
      cy.register();
      cy.get("#form-register-user").within(($form) => {
        cy.get('input[name="username"]').type("username" + Date.now());
        cy.get('input[name="email"]').type("username@mail.com");
        cy.get('input[name="password"]').type("123456789");
        cy.get('input[name="password1"]').type("123456789");
        cy.get('input[name="phone_number"]').type("1".repeat(51));
        cy.get('input[name="country"]').type("Norge");
        cy.get('input[name="city"]').type("Trondheim");
        cy.get('input[name="street_address"]').type("Kongens Gate 1");
      });
      cy.get("#btn-create-account").click();
      cy.contains("Registration failed!");
    });
  
    it("Invalid with too long street address", () => {
      cy.register();
      cy.get("#form-register-user").within(($form) => {
        cy.get('input[name="username"]').type("username" + Date.now());
        cy.get('input[name="email"]').type("username@mail.com");
        cy.get('input[name="password"]').type("123456789");
        cy.get('input[name="password1"]').type("123456789");
        cy.get('input[name="phone_number"]').type("12345678");
        cy.get('input[name="country"]').type("Norge");
        cy.get('input[name="city"]').type("Trondheim");
        cy.get('input[name="street_address"]').type("K".repeat(51));
      });
      cy.get("#btn-create-account").click();
      cy.contains("Registration failed!");
    });
  });
  