describe("Integration test for rating system", () => {
    beforeEach(() => {
      cy.login();
      Cypress.Cookies.preserveOnce("access", "refresh");
    });
  
    it("Valid - Post rating", () => {
      cy.get("#btn-create-workout").click();
  
      cy.get("#inputName").type("Valid workout");
      cy.get("#inputDateTime").type("2022-03-30T12:00");
      cy.get("#inputVisibility").select("PU");
      cy.get("#inputNotes").type("text");
  
      cy.scrollTo("bottom");
  
      cy.get("#div-exercises").within(($form) => {
        cy.get('select[name="type"]').select("1", { force: true });
        cy.get('input[name="sets"]').type("1", { force: true });
        cy.get('input[name="number"]').type("1", { force: true });
      });
  
      cy.get("#btn-ok-workout").click();
      cy.url().should("include", "/workouts.html");
    });


  });
  